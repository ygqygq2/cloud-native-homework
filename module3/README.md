# 作业要求
* 构建本地镜像
* 编写 Dockerfile 将练习 2.2 编写的 httpserver 容器化
* 将镜像推送至 docker 官方镜像仓库
* 通过 docker 命令本地启动 httpserver
* 通过 nsenter 进入容器查看 IP 配置

# 作业过程

![项目目录](https://images.linuxba.com/小书匠/2022-3-1/1646118272976.png)

![docker编译](https://images.linuxba.com/小书匠/2022-3-1/1646118294307.png)

![镜像推送](https://images.linuxba.com/小书匠/2022-3-1/1646118520074.png)

![运行容器查看ip](https://images.linuxba.com/小书匠/2022-3-1/1646118608117.png)

![访问并查看日志](https://images.linuxba.com/小书匠/2022-3-1/1646118681461.png)