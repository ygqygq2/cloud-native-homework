package main

import (
	"fmt"
	"time"
)

func produce(ch chan<- int) {
	for i := 0; i < 20; i++ {
		ch <- i
		time.Sleep(1 * time.Second)
		fmt.Println("Send:", i)
	}
	close(ch)
}

func consume(ch <-chan int) {
	for i := range ch {
		fmt.Println("Receive:", i)
	}
}

func main() {
	ch := make(chan int, 10)
	go produce(ch)
	consume(ch)
	// time.Sleep(1 * time.Second)
}
