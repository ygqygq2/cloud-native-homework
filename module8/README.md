# 作业要求

* 优雅启动
* 优雅终止
* 资源需求和 QoS 保证
* 探活
* 日常运维需求，日志等级
* 配置和代码分离

# 作业过程
`kubectl create -f deploy`

![k8s资源](https://images.linuxba.com/小书匠/2022-4-8/1649408105544.png)

![使用ingress访问](https://images.linuxba.com/小书匠/2022-4-9/1649504690215.png)
