module gitee.com/ygqygq2/httpserver

go 1.17

require (
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
