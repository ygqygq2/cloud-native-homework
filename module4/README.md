# 作业要求

* 启动一个 Envoy Deployment。
* 要求 Envoy 的启动配置从外部的配置文件 Mount 进 Pod。
* 进入 Pod 查看 Envoy 进程和配置。
* 更改配置的监听端口并测试访问入口的变化。
* 通过非级联删除的方法逐个删除对象。

# 作业过程
创建资源，进入 pod ，查看进程

![创建资源，查看信息](https://images.linuxba.com/小书匠/2022-3-9/1646798149224.png)

查看配置
![查看配置](https://images.linuxba.com/小书匠/2022-3-9/1646798180142.png)

通过导出 configmap 成 yaml 文件，修改端口号
![enter description here](https://images.linuxba.com/小书匠/2022-3-9/1646798350653.png)

更新 configmap
![enter description here](https://images.linuxba.com/小书匠/2022-3-9/1646798376920.png)

进入 pod 查看配置是否生效
![enter description here](https://images.linuxba.com/小书匠/2022-3-9/1646798422571.png)

pod 未重启，envoy 程序不会重新加载配置文件
![enter description here](https://images.linuxba.com/小书匠/2022-3-9/1646798558819.png)

重启 pod ，发现 service 访问失败，通过 pod 和新端口号正常访问
![enter description here](https://images.linuxba.com/小书匠/2022-3-9/1646798745493.png)

更新 service 的容器端口号配置
![enter description here](https://images.linuxba.com/小书匠/2022-3-9/1646798792534.png)

service 生效后，重新使用 NodePort 方式访问，正常
![enter description here](https://images.linuxba.com/小书匠/2022-3-9/1646798872641.png)

删除作业资源，先删除 service，清除路由等信息，再删除 deployment，因为它使用了 configmap，最后删除 configmap。
![enter description here](https://images.linuxba.com/小书匠/2022-3-9/1646798952986.png)
