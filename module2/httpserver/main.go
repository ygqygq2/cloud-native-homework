package main

import (
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
)

// 定义健康检测
func healthz(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "ok")
}

// 定义 web 根
func webRoot(w http.ResponseWriter, r *http.Request) {
	// 请求 header
	h := r.Header
	// 获取环境变量 VERSION
	version := os.Getenv("VERSION")
	// header 中加入环境变量
	h.Add("VERSION", version)
	// 将请求 header 转成 返回 header
	for k, v := range h {
		for _, vv := range v {
			w.Header().Add(k, vv)
		}
	}

	clientIP := ClientIP(r)
	// 记录日志
	log.Printf("agent ip: %v, status code: %d\n", clientIP, 200)

	w.Write([]byte("hello world\n"))
}

// ClientIP 尽最大努力实现获取客户端 IP 的算法。
// 解析 X-Real-IP 和 X-Forwarded-For 以便于反向代理（nginx 或 haproxy）可以正常工作。
func ClientIP(r *http.Request) string {
	xForwardedFor := r.Header.Get("X-Forwarded-For")
	ip := strings.TrimSpace(strings.Split(xForwardedFor, ",")[0])
	if ip != "" {
		return ip
	}
	ip = strings.TrimSpace(r.Header.Get("X-Real-Ip"))
	if ip != "" {
		return ip
	}
	if ip, _, err := net.SplitHostPort(strings.TrimSpace(r.RemoteAddr)); err == nil {
		return ip
	}
	return ""
}

func main() {
	http.HandleFunc("/healthz", healthz)

	http.HandleFunc("/", webRoot)

	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal(err)
	}
}
